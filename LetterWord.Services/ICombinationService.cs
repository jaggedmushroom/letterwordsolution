﻿using LetterWord.Logic.Models;
using System.Collections.Generic;

namespace LetterWord.Services
{
    public interface ICombinationService
    {
        IEnumerable<CombinationResult> GetCombinations(List<string> lines);
    }
}
