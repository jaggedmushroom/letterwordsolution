﻿using LetterWord.Logic;
using LetterWord.Logic.Models;
using System.Collections.Generic;

namespace LetterWord.Services
{
    public class CombinationService : ICombinationService
    {
        private readonly ICombinationFinder _combinationFinder;

        public CombinationService(ICombinationFinder combinationFinder)
        {
            _combinationFinder = combinationFinder;
        }

        public IEnumerable<CombinationResult> GetCombinations(List<string> lines)
        {
            _combinationFinder.UseList(lines);
            return _combinationFinder.FindCombinationForWords();
        }
    }
}
