import * as React from "react";
import './Combination.css';

type CombinationProps = {
  word: string;
  combinations: string[];
};

export const Combination = ({ word, combinations }: CombinationProps) => (
  <div key={word} className="row">
    <div><strong>{word}</strong></div>
    <div>{combinations.map((c) => (c + ' | ') )}</div>
  </div>
);
