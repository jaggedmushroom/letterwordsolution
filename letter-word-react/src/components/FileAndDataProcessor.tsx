import * as React from "react";
import axios from "axios";
import { toast } from "react-toastify";
import { CombinationResult } from "./CombinationResult";

export class FileAndDataProcessor extends React.Component<
  {},
  { selectedFiles: FileList; result: any }
> {
  handleChange(selectedFiles: FileList) {
    this.setState({
      selectedFiles: selectedFiles,
    });
  }

  handleClick = () => {
    console.log("Uploading files");

    const data = new FormData();
    data.append("file", this.state.selectedFiles[0]); //We only have a single file in this case

    axios
      .post("/words", data) //Added proxy in package.json to overcome CORS issue with localhost
      .then((res) => {
        toast.success("File has been processed");
        this.setState({
          result: res.data,
        });
      })
      .catch((err) => {
        if (err.response) {
          toast.error(`There was a ${err.response.status} error`);
        }
      });
  };

  render() {
    return (
      <div>
        <input
          type="file"
          onChange={(e) => this.handleChange(e.target.files)}
        />
        <button onClick={this.handleClick}>Process</button>

        {this.state?.result && (
          <CombinationResult
            totalWords={this.state.result.uniqueWordTotal}
            totalCombinations={this.state.result.combinationTotal}
            combinations={this.state.result.wordWithCombinations}
          />
        )}
      </div>
    );
  }
}

export default FileAndDataProcessor;
