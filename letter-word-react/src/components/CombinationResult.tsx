import * as React from "react";
import { Combination } from "./Combination";

type CombinationResultProps = {
  totalWords: number;
  totalCombinations: number;
  combinations: any;
};

export const CombinationResult = ({
  totalWords,
  totalCombinations,
  combinations,
}: CombinationResultProps) => (
  <div>
    <h1>Results</h1>
    <h2>
      There were {totalWords} unique words with {totalCombinations} combinations
    </h2>
    {combinations.map(
      (item: { word: string; combinations: string[]; }) => (
        <Combination word={item.word} combinations={item.combinations} />
      )
    )}
  </div>
);
