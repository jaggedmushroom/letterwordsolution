import React from 'react';
import upload from './upload.gif';
import './App.css';
import FileAndDataProcessor from './components/FileAndDataProcessor';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

class App extends React.Component {

  constructor(props: any) {
    super(props);
    this.state = {
      selectedFiles: null,
      result: null
    };
  }

  render () {
    return (
      <div className="App">
        <header className="App-header">
          <h1>Letter Word Finder</h1>
          <img src={upload} className="App-logo" alt="logo" />
          <p>
            Upload a file
          </p>
          <FileAndDataProcessor />
          <ToastContainer />
        </header>
      </div>
    );
  }
}

export default App;
