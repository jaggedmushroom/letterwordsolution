using System.Linq;
using FluentAssertions;
using LetterWord.Logic;
using Xunit;

namespace LetterWord.Tests
{
    public class CombinationFinderTest
    {
        protected ICombinationFinder CombinationFinder;

        public CombinationFinderTest()
        {
            CombinationFinder = new CombinationFinder();
        }

        [Theory]
        [InlineData(new[] { "ap", "ples", "apples" }, new[] { "ap+ples" })]
        [InlineData(new[] { "a", "honey", "apples", "pp", "les" }, new[] { "a+pp+les"})]
        [InlineData(new[] { "appl", "es", "a", "apples", "pples" }, new[] { "appl+es", "a+pples" })]
        [InlineData(new[] { "honey", "h", "h", "o", "o", "n", "n", "e", "e", "y", "y" }, new[] { "h+o+n+e+y" })]
        public void ShouldFindCombinations(string[] given, string[] expectedCombinations)
        {
            CombinationFinder.UseList(given);

            var result = CombinationFinder.FindCombinationForWords().First().Combinations.ToList();

            expectedCombinations.Length.Should().Be(result.Count);

            foreach (var combination in expectedCombinations)
            {
                result.Should().Contain(combination);
            }
        }

        [Theory]
        [InlineData("ap", "les", "apples")]
        [InlineData("a", "honey", "apples", "les")]
        public void ShouldNotFindCombinations(params string[] given)
        {
            CombinationFinder.UseList(given);

            var result = CombinationFinder.FindCombinationForWords();

            result.Should().BeEmpty();
        }
    }
}
