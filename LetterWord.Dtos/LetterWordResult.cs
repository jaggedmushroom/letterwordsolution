﻿using System.Collections.Generic;

namespace LetterWord.Dtos
{
    public class LetterWordResult
    {
        public LetterWordResult(int uniqueWordTotal, int combinationTotal, List<LetterWordCombination> wordWithCombinations)
        {
            UniqueWordTotal = uniqueWordTotal;
            WordWithCombinations = wordWithCombinations;
            CombinationTotal = combinationTotal;
        }

        public int CombinationTotal { get; }
        public int UniqueWordTotal { get; }
        public List<LetterWordCombination> WordWithCombinations { get; }
    }
}
