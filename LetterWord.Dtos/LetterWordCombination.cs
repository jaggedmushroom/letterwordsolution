﻿using System.Collections.Generic;

namespace LetterWord.Dtos
{
    public class LetterWordCombination
    {
        public LetterWordCombination(string word, List<string> combinations)
        {
            Word = word;
            Combinations = combinations;
        }

        public string Word { get; }
        public List<string> Combinations { get; }
    }
}
