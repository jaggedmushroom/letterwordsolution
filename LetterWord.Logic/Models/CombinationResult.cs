﻿using System.Collections.Generic;

namespace LetterWord.Logic.Models
{
    public class CombinationResult
    {
        public CombinationResult(string word, List<string> combinations)
        {
            Word = word;
            Combinations = combinations;
        }

        public string Word { get; }
        public List<string> Combinations { get; }
    }
}
