﻿using System.Collections.Generic;

namespace LetterWord.Logic.Models
{
    public class CombinationSearchResult
    {
        public CombinationSearchResult(string word, string composedOf)
        {
            Word = word;
            ComposedOf = composedOf;
        }

        public string Word { get; }
        public string ComposedOf { get; }
    }
}
