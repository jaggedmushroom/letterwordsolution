﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LetterWord.Logic.Models
{
    public class WordList
    {
        public int MaxLength { get; private set; }
        private readonly List<string> _wordLookup;
        private readonly Dictionary<int, List<string>> _wordsByLength;

        public IEnumerable<string> GetWordsOfLength(int length)
        {
            return _wordLookup.Where(word => word.Length == length);
        }

        public IEnumerable<string> SearchWordWith(string searchString, int maxLength)
        {
            return _wordsByLength[Math.Min(maxLength, this.MaxLength)]
                .Where(searchString.StartsWith);
        }

        public WordList(IEnumerable<string> words)
        {
            _wordLookup = words.ToList();

            MaxLength = _wordLookup.Max(word => word.Length);

            _wordsByLength = new Dictionary<int, List<string>>();
            for (var i = 1; i <= MaxLength; i++)
            {
                var items = GetWordsOfLength(i).Union(_wordsByLength.SelectMany(x => x.Value));
                _wordsByLength.Add(i, items.ToList());
            }
        }
    }
}
