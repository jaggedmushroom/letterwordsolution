﻿using LetterWord.Logic.Models;
using System.Collections.Generic;

namespace LetterWord.Logic
{
    public interface ICombinationFinder
    {
        void UseList(IEnumerable<string> listOfWords);
        IEnumerable<CombinationResult> FindCombinationForWords();
    }
}
