﻿using LetterWord.Logic.Models;
using System.Collections.Generic;
using System.Linq;

namespace LetterWord.Logic
{
    public class CombinationFinder : ICombinationFinder
    {
        private WordList _wordList;

        public void UseList(IEnumerable<string> words)
        {
            _wordList = new WordList(words);
        }

        public IEnumerable<CombinationResult> FindCombinationForWords()
        {
            var results = _wordList
                .GetWordsOfLength(_wordList.MaxLength)
                .SelectMany(FindCombinationsForWord);

            return results.GroupBy(r => r.Word, p => p.ComposedOf, (keyword, combinations) => 
                new CombinationResult(keyword, combinations.ToList()));
        }

        private IEnumerable<CombinationSearchResult> FindCombinationsForWord(string word)
        {
            return FindCombination(word, word.Length - 1)
                    .Select(combination => 
                        new CombinationSearchResult(word, string.Join("+", combination)));
        }

        private IEnumerable<List<string>> FindCombination(string search, int length)
        {
            foreach (var word in _wordList.SearchWordWith(search, length))
            {
                if (word.Length == search.Length)
                {
                    yield return new List<string> { word };
                }
                else
                {
                    var remainder = search.Substring(word.Length);

                    foreach (var combination in FindCombination(remainder, remainder.Length).Where(c => c.Any()))
                    {
                        combination.Insert(0, word);
                        yield return combination;
                    }
                }
            }
        }
    }
}
