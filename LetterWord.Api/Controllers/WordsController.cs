﻿using LetterWord.Dtos;
using LetterWord.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace LetterWord.Api.Controllers
{
    public class WordsController : BaseController
    {
        private readonly ICombinationService _combinationService;

        public WordsController(ICombinationService combinationService)
        {
            _combinationService = combinationService;
        }

        [HttpPost(Name = "FindWords")]
        public async Task<ActionResult<LetterWordResult>> FindWords(IFormFile file)
        {
            if (file == null)
            {
                return BadRequest("No file was attached");
            }

            var lines = new List<string>();

            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                while (reader.Peek() >= 0)
                {
                    lines.Add(await reader.ReadLineAsync());
                }
            }

            var combinations = _combinationService.GetCombinations(lines)
                .Select(x => new LetterWordCombination(x.Word, x.Combinations)).ToList();

            var result = new LetterWordResult(combinations.Count, combinations.Sum(x => x.Combinations.Count), combinations);

            return Ok(result);
        }
    }
}
