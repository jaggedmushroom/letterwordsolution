﻿using Microsoft.AspNetCore.Mvc;

namespace LetterWord.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public abstract class BaseController : ControllerBase
    {
    }
}
