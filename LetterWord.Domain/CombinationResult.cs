﻿using System.Collections.Generic;

namespace LetterWord.Domain
{
    public class CombinationResult
    {
        public CombinationResult(string word, List<string> composedOf)
        {
            Word = word;
            ComposedOf = composedOf;
        }

        public string Word { get; }
        public List<string> ComposedOf { get; }
    }
}
